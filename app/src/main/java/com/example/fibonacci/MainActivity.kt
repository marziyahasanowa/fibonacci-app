package com.example.fibonacci

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.fibonacci.databinding.ActivityMainBinding
import androidx.fragment.app.commit


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        if (savedInstanceState == null) {
           supportFragmentManager.commit {
               add(R.id.container, MainFragment())
           }
        }
    }

}


