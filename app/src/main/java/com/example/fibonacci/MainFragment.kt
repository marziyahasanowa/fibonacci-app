package com.example.fibonacci

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import com.example.fibonacci.databinding.FragmentMainBinding
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class MainFragment : Fragment() {

    private var isCalculating = false
    private var shouldCancel = false
    private var input = 0
    private var i = 1

    private lateinit var binding: FragmentMainBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMainBinding.inflate(layoutInflater)


        savedInstanceState?.let {
            input = it.getInt("input")
            i = it.getInt("i")
            isCalculating = it.getBoolean("isCalculating")
            shouldCancel = it.getBoolean("shouldCancel")

            if (isCalculating) {
                binding.editText.isEnabled = false
                binding.button.text = getString(R.string.cancel)
                doJob()
            }
        }

        startCalculation()

        return binding.root
    }

    private fun startCalculation() = binding.button.setOnClickListener {
        if (!isCalculating) {
            input = binding.editText.text.toString().toInt()
            i = 1

            binding.editText.isEnabled = false
            isCalculating = true
            shouldCancel = false
            binding.button.text = getString(R.string.cancel)

            doJob()
        } else {
            shouldCancel = true
        }
    }

    @SuppressLint("StringFormatMatches")
    fun doJob(): Job = lifecycleScope.launch(Dispatchers.IO) {
        println(CoroutineName("thread $this"))

        if (shouldCancel) {
            withContext(Dispatchers.Main) {
                isCalculating = false
                shouldCancel = false

                binding.textView.text = ""
                binding.editText.isEnabled = true
                binding.button.text = getString(R.string.start)
            }
            return@launch
        }
        withContext(Dispatchers.Main) {
            binding.textView.text = getString(R.string.current_is, i)
            i++
        }
        if (i <= input) {
            delay(1000)
            doJob()
        } else {
            delay(1000)
            withContext(Dispatchers.Main) {
                binding.textView.text = ""
                binding.textView.text = getString(R.string.result_is, fibonacci(input))

                binding.editText.isEnabled = true
                binding.button.text = getString(R.string.start)

                isCalculating = false
            }
        }
    }

    private fun fibonacci(number: Int): Int {
        var num1 = 0
        var num2 = 1
        var counter = 0
        var sum = 0

        while (counter < number) {
            val temp = num2 + num1
            num1 = num2
            num2 = temp
            counter += 1
            sum = num1 + num2
        }
        return sum
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("input", input)
        outState.putInt("i", i)
        outState.putBoolean("isCalculating", isCalculating)
        outState.putBoolean("shouldCancel", shouldCancel)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        savedInstanceState?.let {
            input = it.getInt("input")
            i = it.getInt("i")
            isCalculating = it.getBoolean("isCalculating")
            shouldCancel = it.getBoolean("shouldCancel")
        }

        if (isCalculating) {
            binding.editText.isEnabled = false
            binding.button.text = getString(R.string.cancel)
            doJob()
        }
    }

}